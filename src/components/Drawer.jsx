import React from "react";
import {
  Drawer as MUIDrawer,
  ListItem,
  List,
  ListItemIcon,
  ListItemText,
  Divider
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";

const useStyles = makeStyles({
  drawer: {
    width: '240',
    flexShrink: 0,
  },
  drawerPaper: {
    width: '240',
  },
  drawerContainer: {
    overflow: 'auto',
  }
});

const Drawer = props => {
  const classes = useStyles();
  const itemsList = [
    {
      text: "Art",
      icon: <InboxIcon />,
      onClick: () => {
        console.log('Indide Art');
      }
    },
    {
      text: "Photography",
      icon: <MailIcon />,
      onClick: () => {
        console.log('Inside Photograpghy');
      }
    },
    {
      text: "Writing",
      icon: <MailIcon />,
      onClick: () => {
        console.log('Inside Writing');
      }
    }
  ];
  return (
    <MUIDrawer className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}>
      <div className={classes.drawerContainer}>
        <List>
          {itemsList.map((item, index) => {
            const { text, icon, onClick } = item;
            return (
              <ListItem button key={text} onClick={onClick}>
                {icon && <ListItemIcon>{icon}</ListItemIcon>}
                <ListItemText primary={text} />
                <Divider />
              </ListItem>
            );
          })}
        </List>
      </div>
    </MUIDrawer>
  );
};

export default Drawer;
