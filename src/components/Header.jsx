import React from "react";
import AppBar from "@material-ui/core/AppBar";
import {
  Toolbar,
  makeStyles,
  Typography,
  IconButton,
  InputBase,
  Grid,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    backgroundColor: "white",
    color: "black",
  },
  logo: {
    color: "black",
  },
  list: {
    position: "relative",
    marginLeft: theme.spacing(10),
    borderBottom: "1px solid black",
    width: "max-content",
    display: "flex",
  },
  gutter: {
    flexGrow: 1,
  },
  search: {
    position: "relative",
    marginLeft: 0,
    marginRight: theme.spacing(10),
    borderBottom: "1px solid black",
    width: "max-content",
    display: "flex",
    // [theme.breakpoints.up("sm")]: {
    //   marginRight: theme.spacing(10),
    //   width: "max-content",
    // },
  },
  appBar: {
    position: 'fixed',
    zIndex: theme.zIndex.drawer + 1400,
  }
}));

function Header() {
  const styles = useStyles();
  return (
    <AppBar position="fixed" >
      <Toolbar className={styles.toolbar}>
        <Typography variant="h5" className={styles.logo}>
          GO.SOCIAL
          </Typography>
        <div className={styles.list}>
          <IconButton size="small">
            <WhatshotIcon />
          </IconButton>
          <Typography variant="h6"> Challenges </Typography>
        </div>
        <Grid item lg={6} md={0}></Grid>
        <div className={styles.search}>
          <InputBase placeholder="Search" />
          <IconButton size="small">
            <SearchIcon />
          </IconButton>
        </div>
        <Avatar src="https://www.androidpolice.com/wp-content/uploads/2017/05/nexus2cee_ic_launcher_play_store_new-1.png"></Avatar>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
