import React from "react";
import Header from "../components/Header";
import { Grid } from "@material-ui/core";

function Home() {
  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Header />
      </Grid>
      <Grid item container spacing={2}>
      </Grid>
    </Grid>
  );
}

export default Home;
