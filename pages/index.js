import React from "react";
import Drawer from "../src/components/Drawer";
import Header from "../src/components/Header";
import { makeStyles } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    display: "flex"
  }
});

export default function App() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline>
        <Header />
        <Drawer />
    </CssBaseline>
    </div>
  );
}
